package com.xebia.domain

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.matchers.ShouldMatchers
import org.scalatest.BeforeAndAfterAll
import org.scalatest.WordSpec
import com.xebia.repo.TestData._
import akka.actor.Actor._
import akka.actor.ActorRef
import akka.actor.ActorSystem
import akka.testkit.TestKit
import akka.util.duration._
import akka.actor.Props
import com.xebia.domain._
import akka.actor.Props
@RunWith(classOf[JUnitRunner])
class DomainTest extends WordSpec with BeforeAndAfterAll with ShouldMatchers {

  "Train" should {
    "stop at a station if it contains it" in {
      train_IC_UTR_AMS_18_30.stopsAt(stationASA) should be(Some(Time("19:45")))
    }
    "go to direction if from station and to station are in route" in {
      train_SPR_UTR_AMS_18_37.goesTo(stationMAS, stationASB) should be(true)
    }
    "not go to direction if from station and to station are not in route" in {
      train_SPR_UTR_AMS_18_37.goesTo(stationASB, stationMAS) should be(false)
    }
  }

  "Journey" should {
    "match train if train applies to route and is within time window" in {
      journeyUTR_AMS_18_20.matches(train_IC_UTR_AMS_18_30) should be(true)
    }
    "not match train if train applies to route and is not within time window" in {
      journeyUTR_AMS_18_20.matches(train_IC_UTR_AMS_19_30) should be(false)
    }
    "not match train if train does not apply to route" in {
      journeyUTR_AMS_18_20.matches(train_ICE_UTR_DHG_18_35) should be(false)
    }

  }

}