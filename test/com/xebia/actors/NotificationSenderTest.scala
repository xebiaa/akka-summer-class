package com.xebia.actors

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.matchers.ShouldMatchers
import org.scalatest.BeforeAndAfterAll
import org.scalatest.WordSpec
import com.xebia.repo.TestData._
import akka.actor.Actor._
import akka.actor.ActorRef
import akka.actor.ActorSystem
import akka.testkit.TestKit
import akka.util.duration._
import akka.actor.Props
import com.xebia.domain._
import akka.actor.Props 
@RunWith(classOf[JUnitRunner])
class NotificationSenderTest extends TestKit(ActorSystem()) with WordSpec with BeforeAndAfterAll with ShouldMatchers {

  val senderActor = system.actorOf(Props(new NotificationSender(testActor)))

  "Notification Sender" should {
    "Receive message within " in {
      within(200 millis) {
        senderActor ! Notification(traveler1, "Delay for train")
        expectMsg("Delay for train to jansen@hotmail.com")
        
      }
    }
  }

}