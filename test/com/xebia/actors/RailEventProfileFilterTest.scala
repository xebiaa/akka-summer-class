package com.xebia.actors

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.matchers.ShouldMatchers
import org.scalatest.BeforeAndAfterAll
import org.scalatest.WordSpec
import com.xebia.repo.TestData._
import akka.actor.Actor._
import akka.actor.ActorRef
import akka.actor.ActorSystem
import akka.testkit.TestKit
import akka.util.duration._
import akka.actor.Props
import com.xebia.domain._
import akka.actor.Props 
import com.xebia.repo._
@RunWith(classOf[JUnitRunner])
class RailEventProfileFilterTest extends TestKit(ActorSystem()) with WordSpec with BeforeAndAfterAll with ShouldMatchers {

  
  override def beforeAll() {
    FilledInMemoryTravelerRepository.init()
  }

  val eventProfileFilterActor = system.actorOf(Props(new RailEventProfileFilter(testActor, FilledInMemoryTravelerRepository)))

  "RailEvent Profile Sender" should {
    "handle DelayEvent event" in {
      within(200 millis) {
        eventProfileFilterActor ! DelayEvent(train_IC_UTR_AMS_18_30, 10)
        expectMsg(Notification(traveler1, "The train to Amsterdam Amstel parting from Utrecht at 18:30 has a delay of 10 minutes"))
        
      }
    }
  }

}