package com.xebia.repo

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.matchers.ShouldMatchers
import org.scalatest.BeforeAndAfterAll
import org.scalatest.WordSpec
import TestData._
import com.xebia.repo.{InMemoryTravelerRepositoryImpl => repo}
@RunWith(classOf[JUnitRunner])
class InMemoryTravelerRepositoryTest extends WordSpec with BeforeAndAfterAll with ShouldMatchers {

  override def beforeAll() {
    repo.truncate()
  }

  "In memory TravelerRepository" should {
    val t1 = traveler1
    "Create train" in {
      repo.saveOrUpdate(t1)
      repo.findById(t1.id) should be(Some(t1))
    }
    "Update train" in {
      repo.saveOrUpdate(t1)
      val t2 = t1.copy(journeys = t1.journeys.init)
      repo.saveOrUpdate(t2) should be(t2)
    }
    "Delete train" in {
      repo.saveOrUpdate(t1)
      repo.remove(t1)
      repo.findById(t1.id) should be(None)
    }
  }

}