package com.xebia.repo

import com.xebia.repo.TestData._

object FilledInMemoryTravelerRepository extends InMemoryTravelerRepository {
  init()
  def init() = {
    truncate()
    travelersList.foreach(t => saveOrUpdate(t))
  }
}

object FilledInMemoryTrainRepository extends InMemoryTrainRepository {
  init()
  def init() = {
    truncate()
    trainsList.foreach(t => saveOrUpdate(t))
  }

}
