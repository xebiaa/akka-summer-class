package com.xebia.repo
import com.xebia.domain._
import java.util.concurrent.atomic.AtomicLong

/**
 * Generic implementation of CRUD actions
 * for in memory repository
 */
trait InMemoryRepository[T <: Persistable[T]] {
  var data: Vector[T]
  val atomicLong = new AtomicLong
  def findById(id: String): Option[T] = data.find(_.id == id) 
  def saveOrUpdate(t: T): T = {
    val toUpdate = if (data.exists(_.id == t.id)) {
      remove(t)
      t
    } else {
      if (t.id.isEmpty)
        t.id(id = atomicLong.incrementAndGet().toString())
      else t
    }
    data = data :+ toUpdate.asInstanceOf[T]
    toUpdate.asInstanceOf[T]
  }
  def remove(t: T) = data = data.filterNot(_.id == t.id)

  def truncate() = data = Vector[T]()

}

/**
 * In memory repository for Train
 * with basic CRUD actions
 */
trait InMemoryTrainRepository extends InMemoryRepository[Train]  with TrainRepository  {
  @volatile var data = Vector[Train]()

}

/**
 * In memory repository for Traveler
 * with basic CRUD actions and more useful operations
 */
trait InMemoryTravelerRepository extends InMemoryRepository[Traveler] with TravelerRepository {
  @volatile var data = Vector[Traveler]()
  def findJourneysForTrain(train: Train): Seq[(Traveler, Journey)] = {
    for {
      traveler <- data
      journey <- traveler.journeys
      if (journey.matches(train))
    } yield (traveler, journey)
  }

}
/** 
 * Directly referable implementation
 */
object InMemoryTrainRepositoryImpl extends InMemoryTrainRepository
/** 
 * Directly referable  implementation
 */
object InMemoryTravelerRepositoryImpl extends InMemoryTravelerRepository

/**
 * In memory repository for all repositories
 */
trait InMemoryRepositoryComponent extends RepositoryComponent {
  override val travelerRepository = InMemoryTravelerRepositoryImpl
  override val trainRepository = InMemoryTrainRepositoryImpl
}

