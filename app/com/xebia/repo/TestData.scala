package com.xebia.repo
import com.xebia.domain._

/**
 * Train that trigger delay notifications:
 * train_IC_UTR_AMS_18_30 for traveler1
 * train_IC_UTR_AMS_19_30 no one
 * train_IC_AMS_UTR_07_50 for traveler 1
 * train_SPR_UTR_AMS_18_37 for traveler1, traveler2
 * train_ICE_UTR_DHG_18_35 for traveler2, traveler3
 */
object TestData {

  val stations = (
    Station("Utrecht"),
    Station("Amsterdam"),
    Station("Amsterdam Amstel"),
    Station("Maarssen"),
    Station("Breukelen"),
    Station("Amsterdam Bijlmer ArenA"),
    Station("Den Haag"))
  val (stationUTR, stationAMS, stationASA, stationMAS, stationBKL, stationASB, stationDHG) = stations

  val trains = (
    Train("ic100", Seq(("18:30", stationUTR), ("19:45", stationASA), ("19:05", stationAMS))),
    Train("ic101", Seq(("19:30", stationUTR), ("20:30", stationASA), ("20:05", stationAMS))),
    Train("ic020", Seq(("07:50", stationAMS), ("08:05", stationASA), ("8:17", stationUTR))),
    Train("spr121", Seq(("18:37", stationUTR), ("18:50", stationMAS), ("19:01", stationBKL), ("19:22", stationASB), ("19:45", stationAMS))),
    Train("ice004", Seq(("18:35", stationUTR), ("20:00", stationDHG))))
  val (train_IC_UTR_AMS_18_30, train_IC_UTR_AMS_19_30, train_IC_AMS_UTR_07_50, train_SPR_UTR_AMS_18_37, train_ICE_UTR_DHG_18_35) = trains

  val journeys = (
    Journey(stationUTR, stationAMS, "18:20"),
    Journey(stationUTR, stationBKL, "18:25"),
    Journey(stationUTR, stationDHG, "18:15"),
    Journey(stationAMS, stationUTR, "7:45"),
    Journey(stationDHG, stationUTR, "8:20"),
    Journey(stationASB, stationUTR, "8:15")
    )
  val (journeyUTR_AMS_18_20, journeyUTR_BKL_18_25, journeyUTR_DHG_18_15, journeyAMS_UTR_07_45, journeyDHG_UTR_08_20, journeyASB_UTR_08_15) = journeys

  val travelers = (
    Traveler("1", "Jansen", "jansen@hotmail.com", Seq(journeyUTR_AMS_18_20, journeyAMS_UTR_07_45)),
    Traveler("2", "Dikson", "d23@online.com", Seq(journeyUTR_BKL_18_25, journeyASB_UTR_08_15, journeyUTR_DHG_18_15)),
    Traveler("3", "Bruinsma", "bruinsma@gmail.com", Seq(journeyUTR_DHG_18_15, journeyDHG_UTR_08_20)))

  val (traveler1, traveler2, traveler3) = travelers

  def asListOf[T](it: Iterator[Any]) = it.map(_.asInstanceOf[T]).toList

  def travelersList = asListOf[Traveler](travelers.productIterator)
  def trainsList = asListOf[Train](trains.productIterator)

}