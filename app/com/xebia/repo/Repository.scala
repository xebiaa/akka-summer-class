package com.xebia.repo
import com.xebia.domain._
import com.xebia.domain.Journey
import com.xebia.domain.Train
import java.util.concurrent.atomic.AtomicLong
/**
 * Generic repository with CRUD actions
 */
trait Repository[T] {
  def findById(id: String): Option[T]
  def saveOrUpdate(t: T): T
  def remove(t: T): Unit
}

/**
 * Traveler Repository interface with
 * basic CRUD actions and more useful operations
 */
trait TravelerRepository extends Repository[Traveler] {
  def findJourneysForTrain(train: Train): Seq[(Traveler, Journey)]
}

/**
 * Train Repository interface with
 * basic CRUD actions and more useful operations
 */
trait TrainRepository extends Repository[Train] 

/**
 * Component combining all repositories
 */
trait RepositoryComponent {
    val travelerRepository: TravelerRepository
    val trainRepository:TrainRepository
}

