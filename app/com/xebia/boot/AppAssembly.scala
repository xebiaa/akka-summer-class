package com.xebia.boot
import com.xebia.actors._
import akka.actor.ActorSystem
import akka.actor.Props
import play.libs.Akka
import com.xebia.repo._
import com.xebia.domain.Traveler
/**
 * Generic assembly trait that wires
 * all actor's together
 */
trait AppAssembly extends RepositoryComponent {    
  val system: ActorSystem
  val endpoint = system.actorOf(Props[WebSocketPushActor])
  val notificationSender = system.actorOf(Props(new NotificationSender(endpoint)))
  val eventProfileFilter = system.actorOf(Props(new RailEventProfileFilter(notificationSender, travelerRepository)))
}

/**
 * Implementation of assembly
 * using the built in Akka system of Play and
 * an inMemoryRepository implementation 
 */
object PlayAppAssembly extends InMemoryRepositoryComponent with AppAssembly  {
  override lazy val system = Akka.system
  initData()
  def initData() {
    import com.xebia.repo.TestData._
    travelersList.foreach(t =>
      travelerRepository.saveOrUpdate(t))
    trainsList.foreach(t =>
      trainRepository.saveOrUpdate(t))
  }

}
