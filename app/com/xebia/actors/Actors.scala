package com.xebia.actors
import com.xebia.domain._
import com.xebia.domain.Notification
import com.xebia.domain.Traveler
import com.xebia.repo._
import akka.actor.actorRef2Scala
import akka.actor.Actor
import akka.actor.ActorRef
/**
 * Matches rail events (delays, track changes etc.) to traveler profiles.
 * For each matching profile notifications are sent to the traveler by
 * means of the NotificationSender
 * Educational goal:
 * - Matching chunks of traveler can be done in parallel
 * - Functions as an aggregator that tracks that all notifications are really sent
 */
class RailEventProfileFilter(sender: ActorRef,  travelerRepo: TravelerRepository) extends Actor {

  import travelerRepo._
  var profiles = Vector[Traveler]()

  def receive = {
    case DelayEvent(train, delay) => {
      findJourneysForTrain(train).foreach {
        case (traveler, journey) =>
          sender ! Notification(traveler, createDelayMsg(journey, train, delay))
      }
    }
  }

  private def createDelayMsg(journey: Journey, train: Train, delay: Int) =
    "The train to %s parting from %s at %s has a delay of %s minutes".
      format(train.route.last.name, journey.from.name, train.stopsAt(journey.from).get, delay)

}
/**
 * Actor for sending notifications to travelers
 * Educational goal:
 * - show that sending can be done concurrently
 */
class NotificationSender(endpoint: ActorRef) extends Actor {

  def receive = {
    case Notification(to, msg) => {
      val notification = "%s to %s" format (msg, to.email)
      endpoint ! notification
    }
  }
}