package com.xebia.domain
import akka.util.Duration


trait Persistable[T] {
  def id:String
  def id(id:String):T
}
//Domain
case class Traveler(id: String, name: String, email: String, journeys: Seq[Journey]) extends Persistable[Traveler]{
  def id(newId:String) = copy(id = newId)
}
object Traveler {
  def apply(name: String, email: String, journeys: Seq[Journey]) = new Traveler("", name, email, journeys)
}

case class Journey(from: Station, to: Station, approxDeparture: Time, departureWindowMinutes: Int = 30) {
  /**
   * Checks whether given train is part of the journey. Criteria:
   * - from and to station are in train's route
   * - train leaves within given window
   */
  def matches(train: Train): Boolean = {
    if (train.goesTo(from, to))
      train.stopsAt(from).map(_.asMinutes - approxDeparture.asMinutes < departureWindowMinutes).getOrElse(false)
    else false
  }
}

case class Train(id: String, schedule: Seq[(Time, Station)]) extends Persistable[Train]{
  def id(id:String):Train = copy(id = id)
  val route = schedule.sortBy(_._1).map(_._2)
  /**
   * Checks whether from and to station are part of trains route
   */
  def goesTo(from: Station, to: Station):Boolean = !route.dropWhile(_ != from).dropWhile(_ != to).isEmpty
  /**
   * Checks whether train stops at given station
   */
  def stopsAt(station: Station): Option[Time] = schedule.find(_._2 == station).map(_._1)
}

case class Station(name: String)

case class Time(hours: Int = 0, minutes: Int = 0) extends Ordered[Time] {
  lazy val asMinutes = hours * 60 + minutes
  override def compare(that: Time) = asMinutes - that.asMinutes
  override val toString = "%02d:%02d".format(hours, minutes)
}

object Time {
  private val timeRegexp = """(\d{1,2}):(\d{1,2})""".r
  def apply(timeString:String):Time = {
	  timeString match {
	    case timeRegexp(hours, minutes) =>  Time(hours.toInt, minutes.toInt)
	    case _ => throw new IllegalArgumentException("%s is not of format %s" format(timeString, timeRegexp.pattern) )
	  }
  }
  implicit def stringToTime(timeString: String): Time = Time(timeString)
}

//Notification messages
case class Notification(to: Traveler, msg: String)

//Rail event messages
sealed trait RailEvent
case class DelayEvent(train: Train, delayMinutes:Int) extends RailEvent
case class TrackChangeEvent(train: Train, at: Station, fromTrack: String, toTrack: String) extends RailEvent
