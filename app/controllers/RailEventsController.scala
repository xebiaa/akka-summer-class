package controllers;

import java.util.concurrent.atomic.AtomicInteger
import com.xebia.actors._
import com.xebia.domain._
import com.xebia.boot._
import akka.actor.ActorRef
import akka.actor.ActorSystem
import akka.actor.Props
import akka.actor.Scheduler
import akka.util.duration._
import play.api.libs.iteratee.Enumerator._
import play.api.libs.iteratee.Enumerator
import play.api.libs.iteratee.Iteratee
import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._

/**
 * Controller for rail events
 */
class RailEventsController( assembly:AppAssembly ) extends Controller {

  import assembly._
 
   /**
   * train Delay form
   */
  val trainDelayFrom: Form[DelayForm] = Form(
    // Define a mapping that will handle User values
    mapping(
      "trainId" -> text,
      "delay" -> number(min = 1, max = 100))
          (DelayForm.apply)(DelayForm.unapply)
  )
 
    /**
   * Handle form submission.
   */
  def submitDelay = Action { implicit request =>
    trainDelayFrom.bindFromRequest.fold(
      errors => Ok("Invalid input provided"),
      delay => {
        val result = trainRepository.findById(delay.trainId) match {
          case Some(train) => 
            eventProfileFilter ! DelayEvent(train, delay.delay)
            "DelayEvent sent successfully"
          case None => "No event sent. Train with id " + delay.trainId + " not found"
        }
       Ok(result) 
      }
    )
  }
  
}

/**
 * Implementations
 */
object RailEventsControllerImpl extends RailEventsController(PlayAppAssembly) 

case class DelayForm(trainId:String, delay:Int)
