package controllers;

import java.util.concurrent.atomic.AtomicInteger
import com.xebia.actors._
import com.xebia.domain._
import com.xebia.boot._
import akka.actor.ActorRef
import akka.actor.ActorSystem
import akka.actor.Props
import akka.actor.Scheduler
import akka.util.duration._
import play.api.libs.iteratee.Enumerator._
import play.api.libs.iteratee.Enumerator
import play.api.libs.iteratee.Iteratee
import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._


/**
 * Controller for initiating websocket connection
 */
class WebSocketController( assembly:AppAssembly ) extends Controller {
  import assembly._
  
  val pusheeCounter = new AtomicInteger

  /**
   * Render initial page
   */   
  def index() = Action {
    Ok(views.html.websockets.index())
  }

  /**
   * Establish websocket connection
   */
  def initWsConnection() = WebSocket.using[String] { request =>
    val in = Iteratee.foreach[String] { msg => endpoint ! PushMessage(msg) }    
    val count = pusheeCounter.incrementAndGet()
    val cpuInfoEnumerator = Enumerator.pushee[String](
      onStart = pushee => endpoint ! AddPushee(count, pushee),
      onComplete = endpoint ! RemovePushee(count),
      onError = (a, b) => println("error " + a + " " + b))
    (in, cpuInfoEnumerator)
  }
  
}

/**
 * Implementations
 */
object WebSocketControllerImpl extends WebSocketController(PlayAppAssembly) 

