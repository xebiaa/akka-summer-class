akka-summerclass
================

Akka Summerclass repo

###Import in Eclipse:
- Run:  
     `sbt`  
	`eclipsify with-source=true`  
- Import project in eclipse (File -> Import -> General -> Existing Projects into Workspace)

###Start Application
- Run:  
     `sbt`  
     `run`
- Access the application under: http://localhost:9000/index

